

/**
 * User model used to create a user and log-in as user
 */
export default class User {

  /**
   * User model
   * Input: email, password, username
   * @constructor
   */
  constructor(
    email,
    password,
    username
  ){ 
    this.email = email;
    this.password = password;
    this.username = username;
  }


}