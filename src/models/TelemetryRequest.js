
// Rappresents a telemetry data
export default class TelemetryRequest {

    // Constructor class
    constructor(
        devicename,
        mesaurename,
        lessrecenttime,
        morerecenttime
    ){  
        // Attributes...
        // Device name
        this.deviceName = devicename;
        // Mesaure name
        this.mesaureName = mesaurename;
        // Start window time
        this.lessRecentTime = lessrecenttime;
        // End window date
        this.moreRecentTime = morerecenttime;
        // ...
    }
  
  
  }