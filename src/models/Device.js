import { stat } from "fs";

// Rappresent an IoTHub registered device
export default class Device {

  // Constructor class
  constructor(
    name, 
    connected,
    connectedAtLeastOneTime,
    connectedLastTime,
    state,
    connectionString,
    userId,
    privateKey
  ){  
    // Attributes...
    // Device name
    this.name = name;
    // Is connected device
    this.connected = connected;
    // Connected physically at list one time
    this.connectedAtLeastOneTime = connectedAtLeastOneTime;
    // Last connection time 
    this.connectedLastTime = connectedLastTime;
    // Device state
    this.state = state;
    // Device connection string
    this.connectionString = connectionString;
    // Admin username 
    this.userId = userId
    // Private key
    this.privateKey = privateKey
  }


}