
// Rappresents a telemetry data
export default class TelemetryData {

    // Constructor class
    constructor(
        devicename,
        date,
        content
    ){  
      // Attributes...
      // Device name
      this.devicename = devicename;
      // Invoming data date
      this.time = date;
      // Raw data
      this.data = content;
      // ...
    }
  
  
  }