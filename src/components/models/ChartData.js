
// Rappresent data for chart
export default class ChartData {

    // Constructor class
    constructor(
        mesaurename,
        mesaureunit,
        xvalues,
        yvalues
    ){  
        // Attributes...
        this.mesaurename = mesaurename;
        this.mesaureunit = mesaureunit;
        this.xvalues = xvalues;
        this.yvalues = yvalues;
      // ...
    }
  }