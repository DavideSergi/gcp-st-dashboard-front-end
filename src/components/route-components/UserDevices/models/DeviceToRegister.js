

// Device to register
export default class DeviceToRegister {


    // Constructor class
    constructor(
        id,
        createDevice,
    ){  
        this.id = id;
        this.new = createDevice; 
    }
}
