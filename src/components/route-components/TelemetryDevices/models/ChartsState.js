
/**
 * Charts state model
 */
export default class ChartsState {

    /**
     * 
     * @param {*} windowTime
     * @param {*} mesaureName
     * @param {*} isRealtime
     */ 
    constructor(
        windowTime,
        mesaureName,
        isRealtime,
    ){  
        this.windowTime = windowTime;
        this.mesaureName = mesaureName;
        this.isRealtime = isRealtime;
    }
}