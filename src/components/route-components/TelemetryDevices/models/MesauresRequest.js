// Mesaure request model products by TelemetryDataContent component
export default class MesauresRequest {

    // Constructor class
    constructor(
        // List of device names
        devicenames,
        // Mesaure name
        mesaurename,
        // More recent time instant of window
        windowstart,
        // Last time instant of window
        windowend,
        // Relatime: true == startRealtime, false == stopRelatime, undefined == noRealtime
        realtime
    ){  
        this.devicenames = devicenames;
        this.mesaurename = mesaurename;
        this.windowstart = windowstart;
        this.windowend = windowend;
        this.realtime = realtime;
    }
}