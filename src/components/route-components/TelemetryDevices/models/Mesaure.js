// Mesaure model products by TelemetryDevice component,
// handled by TelemetryDevices bridge,
// and consumed by TelemetryDataContent
export default class Mesaure {

    // Constructor class
    constructor(
        devicename,
        mesaurename,
        unit,
        accurancy,
        commondevicename,
        max,
        min
    ){  
        this.devicename = devicename;
        this.mesaurename = mesaurename;
        this.unit = unit;
        this.accurancy = accurancy;
        this.commondevicename = commondevicename;
        this.max = max;
        this.min = min;
    }
}