
import Vue from 'vue';
import AuthenticationService from '@/services/AuthenticationService';

const authService = new AuthenticationService();
const DEBUG =  undefined

// Provides all REST Uri for application        
export default class RestEndpoints {

    // Constructor class
    constructor() {

        // Base address rest interface
        if(DEBUG)
            this.base = "http://localhost:8010/stm32iot-206219/us-central1";
        else
            this.base = "https://europe-west1-stm32iot-206219.cloudfunctions.net";

        this.DeviceManagerUrl = this.base+"/DeviceManager";
        this.TelemetryUrl = this.base+"/Telemetry";
        // ....
        
    }
    // Create a new device at cloud side
    CreateDevice(deviceId,userId,publicKey,privateKey,success,error){

        var promise = authService.GetAuthenticationToken();
        promise
            .then(
                function(token){
                    var url = this.DeviceManagerUrl+"/registerDevice";
                    
                    var body = {
                        "deviceId" : deviceId,
                        "userId":userId,
                        "publicKey" : publicKey,
                        "privateKey" : privateKey
                    }

                    Vue.http.headers.common['Authorization'] ="Bearer "+token;

                    Vue.http.post(url,body)
                    .then(
                        (response) => {
                            // Success
                            success(response.data)
                        },
                        (response) => {
                            // Error
                            error(response)
                            }
                        )  
                     }.bind(this)
            )
            .catch(
                function(err){
                    error(err);
                }.bind(this)
            );
    }
    // Check existing device
    CheckDevice(deviceid,success,error){
        var promise = authService.GetAuthenticationToken();
        // Error
        promise
            .then(
                function(token){
                    
                    Vue.http.headers.common['Authorization'] ="Bearer "+token;
                    var url = this.DeviceManagerUrl+"/checkExist"+"?deviceId="+deviceid;
                    
                    Vue.http.get(url).then(
                        (response) => {
                            // Success
                            success(true)
                        },
                        (response) => {
                            // Error
                            if(response.status == 404){
                                success(false)
                                return;
                            }
                            // ... implement something for other specific response code ....

                            // Error
                            error(response)
                        }
                    );   
                }.bind(this)
            )
            .catch(
                function(err){
                    error(err);
                }.bind(this)
            );
    }
    // Get an existing device at cloud side
    GetDevice(deviceid,success,error){
        var promise = authService.GetAuthenticationToken();
        // Error
        promise
            .then(
                function(token){
                    Vue.http.headers.common['Authorization'] ="Bearer "+token;
                    var url =  this.DeviceManagerUrl+"/getDevice"+"?deviceId="+deviceid;
                    Vue.http.get(url).then(
                      (response) => {
                          // Success
                          success(response.data)
                      },
                      (response) => {
                          // Error
                          error(response)
                        }
                    );
                }.bind(this)
            )
            .catch(
                function(err){
                    error(err);
                }.bind(this)
            );
    }
    // Get an existing device at cloud side
    GetDevices(success,error){

        var promise = authService.GetAuthenticationToken();
        promise
            .then(
                function(token){
                    Vue.http.headers.common['Authorization'] ="Bearer "+token;
                    var url =  this.DeviceManagerUrl+"/getDevices";
                    Vue.http.get(url).then(
                      (response) => {
                          // Success
                          success(response.data)
                      },
                      (response) => {
                          // Error
                          error(response)
                        }
                    );
                }.bind(this)
            )
            .catch(
                function(err){
                    error(err);
                }.bind(this)
            );

    }

    // Update device state
    UpdateDeviceState(deviceid,state,success,error){

        var promise = authService.GetAuthenticationToken();
        promise
            .then(
                function(token){
                    Vue.http.headers.common['Authorization'] ="Bearer "+token;
                    var url = this.DeviceManagerUrl+"/updateDeviceState";
                    var body = {
                        deviceId : deviceid,
                        state : state
                    }
                    Vue.http.put(url,body).then(
                      (response) => {
                          // Success
                        success(response.data)
                      },
                      (response) => {
                          // Error
                          error(response)
                        }
                    );
                }.bind(this)
            )
            .catch(
                function(err){
                    error(err);
                }.bind(this)
            );
    }

    // TODO: to change urls
    
    // Get telemetry data per mesaure and per device
    GetTelemetryData(
        deviceNames,
        mesaureName,
        lessRecentTime,
        moreRecentTime,
        success,
        error
    ){
    
    
        var promise = authService.GetAuthenticationToken();
        promise
            .then(
                function(token){
                    Vue.http.headers.common['Authorization'] ="Bearer "+token;
                    if(mesaureName==null){
                        var url = this.TelemetryUrl+"/mesaures/devices";
                        url = url + "?deviceIds=" + deviceNames + "&";
                        url = url + "start=" + lessRecentTime + "&";
                        url = url + "end=" + moreRecentTime;
            
                        Vue.http.get(url).then(
                            (response) => {
                                // Success
                              success(response.data)
                            },
                            (response) => {
                                // Error
                                error(response)
                              }
                          );
            
                    }else{
                        // Call mesaure specific
                        var url = this.TelemetryUrl+"/mesaures/devices";
                        url = url + "?deviceIds=" + deviceNames + "&";
                        url = url + "mesaure=" + mesaureName + "&";
                        url = url + "start=" + lessRecentTime + "&";
                        url = url + "end=" + moreRecentTime;
            
                        Vue.http.get(url).then(
                            (response) => {
                                // Success
                              success(response.data)
                            },
                            (response) => {
                                // Error
                                error(response)
                              }
                          );
                    }
                }.bind(this)
            )
            .catch(
                function(err){
                    error(err);
                }.bind(this)
            );
    }

    // Subscribe to user topic to listen notification
    StartListeningTelemetryMessage(registrationToken,success,error){

        var promise = authService.GetAuthenticationToken();
        promise
            .then(
                function(token){
                    Vue.http.headers.common['Authorization'] ="Bearer "+token;
                    var url = this.TelemetryUrl+"/mesaures/start-listen";
                    var body = {
                        registrationToken : registrationToken
                    }
                    Vue.http.post(url,body).then(
                      (response) => {
                          // Success
                        success(response.data)
                      },
                      (response) => {
                          // Error
                          error(response)
                        }
                    );
                }.bind(this)
            )
            .catch(
                function(err){
                    error(err);
                }.bind(this)
            );
    }
    // Unsubscribe to user topic to listen notification
    StopListeningTelemetryMessage(registrationToken,success,error){

        var promise = authService.GetAuthenticationToken();
        promise
            .then(
                function(token){
                    Vue.http.headers.common['Authorization'] ="Bearer "+token;
                    var url = this.TelemetryUrl+"/mesaures/stop-listen";
                    var body = {
                        registrationToken : registrationToken
                    }
                    Vue.http.post(url,body).then(
                      (response) => {
                          // Success
                        success(response.data)
                      },
                      (response) => {
                          // Error
                          error(response)
                        }
                    );
                }.bind(this)
            )
            .catch(
                function(err){
                    error(err);
                }.bind(this)
            );
    }
    // ...
}