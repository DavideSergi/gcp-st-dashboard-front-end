// Application imports
import Device from '@/models/Device';


// Provides all REST Uri for application        // FIXME : not only rest endpoints
export default class NetworkTypeConverter {



    // Read a json raw device and converts it into
    // a Device application instance.
    DeserializeDevice(rawDevice){

        var nowdate = new Date((new Date()).getTime()-75*1000);
        var utcnowdate = new Date(nowdate.toUTCString());
        
        var heartbeatdate = new Date(rawDevice.lastHeartbeatTime);
        var utcheartbeatdate = new Date(heartbeatdate.toUTCString());
        var lastEventTimeDate = new Date(rawDevice.lastEventTime);
        var utcLastEventTimeDate = new Date(lastEventTimeDate.toUTCString());
        var connectedDate = (utcLastEventTimeDate.getTime()>utcheartbeatdate.getTime())?utcLastEventTimeDate:utcheartbeatdate;
        // console.log("[NetworkTypeConverter:DeserializeDevice] raw last event time date")
        // console.log(rawDevice.lastEventTime);
        // console.log("[NetworkTypeConverter:DeserializeDevice] last event time date")
        // console.log(lastEventTimeDate);
        // console.log("[NetworkTypeConverter:DeserializeDevice] heart beat date")
        // console.log(heartbeatdate)

        var name = rawDevice.id
        // if(name==="Canestrello"){
        //     // console.log("[NetworkTypeConverter::DeserializeDevice] Heartbeat: "+utcheartbeatdate);
        //     // console.log("[NetworkTypeConverter::DeserializeDevice] Now: "+utcnowdate);
        //     // console.log("[NetworkTypeConverter::DeserializeDevice] Diff: "+(utcnowdate.getTime()-utcheartbeatdate.getTime()))
        // }
        var connected = ((utcnowdate.getTime()-connectedDate.getTime()>70*1000))?false:true; // Last heartbeat message based timestamp
        var connectedAtLeastOneTime = (rawDevice.lastSeenTime!=null||rawDevice.lastHeartbeatTime!=null)?true:false;
        var connectedLastTime = null;
        var state = null;
        var connectionString = rawDevice.connectionString;
        var privateKey = rawDevice.privateKey;
        if(connectedAtLeastOneTime===true)
            connectedLastTime = new Date(rawDevice.lastSeenTime);

        if(connectedAtLeastOneTime === false) connected = false;

        if(rawDevice.binaryState!=null)
            state = JSON.parse(Buffer.from(rawDevice.binaryState, 'base64').toString());
        else
            state = { }

        // var state = null;
        // for(var index in rawDevice.deviceState.deviceStates)
        //     state = rawDevice.deviceState.deviceStates[index];
        // if(state!=null)
        //     state = JSON.parse(Buffer.from(state.binaryData, 'base64').toString());
        // else
        //     state = { }

        // Build applcation device
        var d = new Device(    
            name,
            connected,
            connectedAtLeastOneTime,
            connectedLastTime,
            state,
            connectionString,
            undefined,
            privateKey
        );

       
        // console.log(d)
        return d;
    }


    //...
}
