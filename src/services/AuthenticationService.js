
// Application imports
import Settings from '@/settings/settings'
// Need to call first to inizialize firebase and so on...
const settings = new Settings();
// Import Firebase
const Firebase = require('firebase'); // == import firebase from 'firebase'

/**
** Provides user manager capabilities
*/
export default  class AuthenticationService{

    /**
     * @constructor
     */
    constructor(){
        this.isLoggedIn = false;
        Firebase.auth().onAuthStateChanged(
            // Default observer
            function(user){
                if(user)
                    this.isLoggedIn = true;
                else    
                    this.isLoggedIn = false;
            }.bind(this)
        );
    }
    /**
     * Create new user given user crediantials (email, password method)
     * @param {*} success
     * @param {*} error 
     */
    CreateUser(user, success, error){
        var email = user.email;
        var password = user.password;
        var username = user.username;

        Firebase.auth().createUserWithEmailAndPassword(email,password)
        .then(function(userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            success(userRecord);
        })
        .catch(function(err) {
            error(err);
        });
    } 
    /**
     * Login given user credentials (email, password method)
     * @param {*} success
     * @param {*} error 
     */
    Login(user,success,error){
        var email = user.email;
        var password = user.password;
        // Sign-in
        Firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function(userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            success(userRecord);
        })
        .catch(function(err) {
            error(err);
        });
    }
    /**
     * Logout from current user session
     * @param {*} success
     * @param {*} error 
     */
    Logout(success,error){
        Firebase.auth().signOut()
        .then(function(data){
            success(data);
        })
        .catch(function(err){
            error(err)
        });
    }
    /**
     * Retrieve authentication if user is logged in
     * @param {*} success
     * @param {*} error 
     */
    GetAuthenticationToken(success,error){
        // if(this.isLoggedIn === true)
        //     Firebase.auth().currentUser.getIdToken()
        //     .then((token)=>{
        //         success(token);
        //     })
        //     .catch((err)=>{
        //         error(err);
        //     });
        // success(null);
        var p = null;
        console.log(this.isLoggedIn);
        if(this.isLoggedIn === true)
            var p = new Promise(
                function(resolve, reject){
                Firebase.auth().currentUser.getIdToken()
                .then((token)=>{
                    resolve(token);
                })
                .catch((err)=>{
                    reject(err);
                });
            })
        return p;
    }

    /**
     * Observer for login/logout user session
    */
    SetUserSessionObserver(observer){
        Firebase.auth().onAuthStateChanged(function(user){
            this.isLoggedIn = (user)?true:false; 
            var username = (user)?(user.email.split('@'))[0]:null;
            observer(username);
        }.bind(this));
    }
}