import Vue from 'vue';
// Application imports
import Settings from '@/settings/settings'
// Need to call first to inizialize firebase and so on...
const settings = new Settings();
// Import Firebase
const Firebase = require('firebase'); // == import firebase from 'firebase'
// Application imports
import RestEndpointsService from '@/settings/RestEndpoints';
import TelemetryData from '@/models/TelemetryData';
import AuthenticationService from './AuthenticationService';

// Instancing services
const appUrlService = new RestEndpointsService();

// Provides capabilities about registration device
export default class DeviceService {

    /**
     * @constructor
     */
    constructor(){
        // Retrieve Firebase Messaging object.
        this.messaging = Firebase.messaging();

        // TO MOVE IN SETTINGS
        navigator
            .serviceWorker
            .register('/sw.js')
            .then(
                function(registration){
                    this.messaging.useServiceWorker(registration);
                    this.messaging.usePublicVapidKey("BP4UFNsAdYg5opJeiButJIbaej1NRr87Ccnr_pdxhxsOs8nlhjYZouoQp-7GMRDOtw6GKSp59DBbsLad8kTrNnY");
                    this.messaging.requestPermission();        
 
                }.bind(this)
            ).catch(err => {
                console.log(err)
            })
        

        
        // // Get Instance ID token is updated.
        // this.messaging
        //     .getToken()
        //     .then(function(currentToken) {
        //         if (currentToken) {
        //             this.token = currentToken;
        //         } else {
        //             // Show permission request.
        //             console.log('No Instance ID token available. Request permission to generate one.');
        //         }
        //     }.bind(this))
        //     .catch(function(err) {
        //         console.log('An error occurred while retrieving token. ', err);
        //     }.bind(this));


        // // Callback fired if Instance ID token is updated.
        // this.messaging.onTokenRefresh(
        //     function() {
        //         this.messaging
        //             .getToken()
        //             .then(function(refreshedToken) {
        //                 console.log('Token refreshed.\n( '+refreshedToken+' )');
        //                 this.token = refreshedToken;
        //             }.bind(this))
        //             .catch(function(err) {
        //                 console.log('Unable to retrieve refreshed token ', err);
        //             }
        //         );
        //     }.bind(this)
        // );
    }

    // Update device state
    UpdateStateDevice(device,patch,success,error){
        // ,device.state
        appUrlService.UpdateDeviceState(device.name,patch,success,error);
    }

    // Fetch telemetry data
    FetchTelemetryData(telemetryRequest,success, error){

        // // Handle DST changing telemetry request
        // var date = new Date()
        // if (date.isDstObserved()) { 
        //     var less = new Date(Date.parse(telemetryRequest.lessRecentTime));
        //     var more = new Date(Date.parse(telemetryRequest.moreRecentTime));

        //     less.setHours(less.getHours()+1)
        //     more.setHours(more.getHours()+1)

        //     telemetryRequest.lessRecentTime = less.toISOString()
        //     telemetryRequest.moreRecentTime = more.toISOString()
        //     // Log
        //     console.log("[DeviceService:FetchTelemetryData] DST triggered. Adjusting date ("+date.isDstObserved()+")");
        // }else{

        //     // var less = new Date(Date.parse(telemetryRequest.lessRecentTime));
        //     // var more = new Date(Date.parse(telemetryRequest.moreRecentTime));

        //     // var lessPatchedMillis = less.setHours(less.getHours())
        //     // var morePatchedMillis = more.setHours(more.getHours())
        //     // less = new Date(lessPatchedMillis);
        //     // more = new Date(morePatchedMillis);

        //     // telemetryRequest.lessRecentTime = less.toISOString()
        //     // telemetryRequest.moreRecentTime = more.toISOString()
        //     console.log("[DeviceService:FetchTelemetryData] DST not triggered ("+date.isDstObserved()+")");
        // }     
        // // \ Handle DST 


        var successCallback =  function(telemetrydata){
            var parsedtelemetrydata = [];
            telemetrydata.forEach(data =>{

                var devicename = null;
                var time = null;
                var content = {};

                for(var index in data){
                    if(index === 'timestamp'){
                        time = data[index];
                        continue;
                    }
                    if(index === 'id'){
                        devicename= data[index];
                        continue;
                    }
                    //FIXME: check if it's correct to get string as telemetry data
                    content[index] = Number(data[index]);
                } 

                var parseddata = new TelemetryData(
                    devicename,
                    time,
                    content
                );
                parsedtelemetrydata.push(parseddata);
            });
            success(parsedtelemetrydata);
        }

        var deviceNames = ""
        var deviceNameCount = telemetryRequest.deviceName.length;
        telemetryRequest.deviceName.forEach(name=>{ 
            deviceNames+= name
            deviceNameCount--
            if(deviceNameCount===0)
                return;
            deviceNames+=" ";
        });

        appUrlService.GetTelemetryData(
            deviceNames,//telemetryRequest.deviceName,
            telemetryRequest.mesaureName,
            telemetryRequest.lessRecentTime,
            telemetryRequest.moreRecentTime,
            successCallback,
            error
        );
    }

    // Start to listen incoming telemetry data of user devices
    StartListeTelemetryRealtimeData(successMessageReceived,error){

        var successCallback = function(data){
            console.log('[DeviceService] Listening started')
            console.log(data);
            // Foreground handling messages...
            this.messaging.onMessage(function(payload) {
                console.log('[DeviceService] Message received. ', payload);
                var parsedtelemetrydata = [];
                var telemetrydata = [JSON.parse(payload.data.message)];
                telemetrydata.forEach(data =>{
    
                    var devicename = null;
                    var time = null;
                    var content = {};
    
                    for(var index in data){
                        if(index === 'timestamp'){
                            time = data[index];
                            continue;
                        }
                        if(index === 'id'){
                            devicename= data[index];
                            continue;
                        }
                        //FIXME: check if it's correct to get string as telemetry data
                        content[index] = Number(data[index]);
                    } 
                
                    var parseddata = new TelemetryData(
                        devicename,
                        time,
                        content
                    );
                    // Check duplicated
                    var found = false;
                    parsedtelemetrydata.forEach(pushedData =>{
                        if(
                            pushedData.devicename===devicename  &&
                            pushedData.time===time
                        )
                            found = true;
                    });
                    if(found===false)
                        parsedtelemetrydata.push(parseddata);
                });

                // console.log(parsedtelemetrydata)
                successMessageReceived(parsedtelemetrydata);
            });
            // Background handling messages...
            // ...
        }.bind(this);
        var errorCallback = function(err){
            console.log('[DeviceService] Error: unable to start listening')
            console.log(err);
            error();
        };


        this.token = null;
        // Get Instance ID token is updated.
        this.messaging
            .getToken()
            .then(function(currentToken) {
                if (currentToken) {
                    appUrlService.StartListeningTelemetryMessage(currentToken,successCallback,errorCallback);
                    
                } else {
                    // Show permission request.
                    console.log('[DeviceService] No Instance ID token available. Request permission to generate one.');
                }
            }.bind(this))
            .catch(function(err) {
                console.log('[DeviceService] An error occurred while retrieving token. ', err);
                error(err);
            }.bind(this));
    }
    // Start to listen incoming telemetry data of user devices
    StopListeTelemetryRealtimeData(success,error){
        
        var successCallback = function(data){
            console.log('[NotificationHubService] Listening stopped')
            console.log(data);
            // ...
        }.bind(this);
        var errorCallback = function(err){
            console.log('[NotificationHubService] Error: unable to stop listening')
            console.log(err);
        };


        this.token = null;
        // Get Instance ID token is updated.
        this.messaging
            .getToken()
            .then(function(currentToken) {
                if (currentToken) {
                    appUrlService.StopListeningTelemetryMessage(currentToken,successCallback,errorCallback);
                    
                } else {
                    // Show permission request.
                    console.log('No Instance ID token available. Request permission to generate one.');
                }
            }.bind(this))
            .catch(function(err) {
                console.log('An error occurred while retrieving token. ', err);
            }.bind(this));
    }

}