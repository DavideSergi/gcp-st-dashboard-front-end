import Settings from '@/settings/settings'
var settings = new Settings();
// Application imports
import RestEndpointsService from '@/settings/RestEndpoints';
import AuthenticationService from './AuthenticationService';
import NetworkTypeConverterService from '@/settings/NetworkTypeConverter'
import NodeRSA from 'node-rsa'
// Instancing services
const appRestService = new RestEndpointsService();
const networkTypeConverterService = new NetworkTypeConverterService();
const authService = new AuthenticationService();
/** 
 * Provides capabilities about registration device
 */
export default class RegistrationDeviceService {

    /**
     * Register a new device under a given group
     * @param newdevice new device model ( @type NewDevice )
     * @param success success callback ( @type function )
     * @param error error callback ( @type function ) 
     */
    RegistersDevice(newdevice, success, error){
        // Save an object
        var saveData = function (data, fileName) {    
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            var 
                blob = new Blob([data], {type: "octet/stream"}),
                url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = fileName;
            a.click();
            window.URL.revokeObjectURL(url);
        }

        // Create or retrieve device
        var successCallback = function(exist){
            if(exist===true)
            {
                // console.log("[RegistrationDeviceService::RegisterDevice] Device id already exist.");
                error({applicationAppError : "Device id already exist"});
                return;
            }

            var successCallback = function(){
                saveData(privateKey,filename);
            }.bind(this);
            var errorCallback = function(err){
                error(err);
            }.bind(this);

            const key = new NodeRSA({b: 2048});
            var publicKey = key.exportKey('pkcs8-public-pem');
            var privateKey = key.exportKey('pkcs8-private-pem');
            // var filename = newdevice.name+"-rsa-private.pem";
            //saveData(privateKey,filename);
            
            var successCallback = function(){
                var successCallback = function(fetchedDevice){
                    var deserializedFetchedDevice = networkTypeConverterService.DeserializeDevice(fetchedDevice);
                    success(deserializedFetchedDevice);
                };
                var errorCallback = function(){
                    error(err);
                };

                appRestService.GetDevice(
                    newdevice.name,
                    successCallback,
                    errorCallback
                );
                // saveData(privateKey,filename);
            }.bind(this);

            var errorCallback = function(err){
                error(err);
            }.bind(this);

            appRestService.CreateDevice(
                newdevice.name,
                newdevice.userId,
                publicKey,
                privateKey,
                successCallback,
                errorCallback
            );
        }.bind(this);

        // Check exiting device identity
        appRestService.CheckDevice(newdevice.name,successCallback,error);


        // if(newdevice.new === true){
        //     var successCallback = function(){
        //         const key = new NodeRSA({b: 2048});
        //         var publicKey = key.exportKey('pkcs8-public-pem');
        //         var privateKey = key.exportKey('pkcs8-private-pem');
        //         var filename = newdevice.id+"-rsa-private.pem";
        //         saveData(privateKey,filename);
        //         appRestService.CreateDevice(newdevice.id,publicKey,success,error);
        //     }.bind(this);
        //     appRestService.CheckDevice(newdevice.id,error,successCallback);
        // }else{
        //     var successCallback = function(data){
        //         var device = networkTypeConverterService.DeserializeDevice(data);
                
        //         success(device);
        //     };
        //     appRestService.GetDevice(newdevice.id,successCallback,error);
        // }
    } 

    /**
     * Register a new device under a given group
     * @param devices new device model ( @type Device[] )
     * @param success success callback ( @type function )
     * @param error error callback ( @type function ) 
     */
    GetDevices(devices,success, error){
        var successCallback =  function(rawDevices){
            var deserializedDevices = [];
            for(var index in rawDevices){
                var rawDevice  = rawDevices[index];
                var deserializedDevice = networkTypeConverterService.DeserializeDevice(rawDevice);
                deserializedDevices.push(
                    deserializedDevice
                );
            }
            success(deserializedDevices);
        }
        var errorCallback = function(err){
            error(err);
        };

        if(!devices||devices===null)
            appRestService.GetDevices(
                successCallback,
                errorCallback);
        else
            console.log("[RegistrationDeviceService::GetDevices] not implemented yet")
    }

    // Return connection string
    GetDeviceConnectionString(device){

    }


    /**
     * Download Comodo cert for TLS server authN
     */ 
    DownloadTLSServer(){
        // Save an object
        var saveData = function (data, fileName) {    
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            var 
                blob = new Blob([data], {type: "octet/stream"}),
                url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = fileName;
            a.click();
            window.URL.revokeObjectURL(url);
        }
        var filename = "TLSServerCert.pem";
        var comodoCertFile = settings.ComodoCert;
        saveData(comodoCertFile,filename); 

    }
  }