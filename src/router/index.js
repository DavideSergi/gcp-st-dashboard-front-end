import Vue from 'vue'
import Router from 'vue-router'
import Vuetify from 'vuetify'
import UserDevices from '@/components/route-components/UserDevices'
//import TelemetryDevices from '@/components/route-components/TelemetryDevices'
import NewTelemetryDevices from '@/components/route-components/NewTelemetryDevices'
import DeviceDetailsContainer from '@/components/route-components/DeviceDetailsContainer'

Vue.use(Router, Vuetify)

// Vue routing settings
export default new Router({
  // Set of routes
  routes: [
    // User devices view (default)
    {
      // Default state of single page application
      path: '/devices',
      name: 'UserDevices',
      props: true, 
      component: UserDevices,
      children:[
        {
          // DeviceDetails will be rendered inside UserDevices <router-view>
          // when /:id/profile is matched
          path: ':id',
          name: 'DeviceDetails',
          //component: DeviceDetails,
          props: true, 
          component: DeviceDetailsContainer,

        }
      ]
    },
    // // Telemetry devices view
    // {
    //   path: '/telemetry',
    //   name: 'TelemetryDevices',
    //   component: TelemetryDevices,
    // },
    // Telemetry devices view
    {
      path: '/telemetry',
      name: 'NewTelemetryDevices',
      component: NewTelemetryDevices,
    },
    
  ]
})
