import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueResource from 'vue-resource'
// Application imports
import Settings from "@/settings/settings"  
import RestEndpoints from '@/settings/RestEndpoints';
import NetworkTypeConverter from '@/settings/NetworkTypeConverter';
import DeviceService from '@/services/DeviceService';
import RegistrationDeviceService from '@/services/RegistrationDeviceService';
import AuthenticationService from '@/services/AuthenticationService'
// Global service instaces
// Rest endpoints handle
const restEndpointsService= new RestEndpoints();
// Nework type converter handle
const networkTypeConverterService = new NetworkTypeConverter();
// Registration device service
const registrationService = new RegistrationDeviceService();
// Device service
const deviceService = new DeviceService();
// Authentication service 
const authenticationService = new AuthenticationService();
// Settings service 
const settingsService = new Settings();
// \Global service instaces
// Building and install service object for global access from vue application
const serviceshandle = {
    // API rest endpoints 
  AppUrl: restEndpointsService,
  // Network type object converter
  NetworkTypeConverter : networkTypeConverterService,
  // Registration service
  Registration : registrationService,
  // Device service
  Device : deviceService ,
  // Authentication user service
  Authentication: authenticationService,
  // Settings
  Settings: settingsService
}

serviceshandle.install = function(){
  Object.defineProperty(Vue.prototype, 'services', {
    get () { return serviceshandle }
  })
}

Vue.use(serviceshandle);
// \ Building and install service object for global access from vue application


Vue.config.productionTip = false
Vue.use(VueResource);

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')


// DST (Daylight Saving Time observer)
Date.prototype.stdTimezoneOffset = function () {
  var jan = new Date(this.getFullYear(), 0, 1);
  var jul = new Date(this.getFullYear(), 6, 1);
  return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}

Date.prototype.isDstObserved = function () {
  return this.getTimezoneOffset() < this.stdTimezoneOffset();
}


// var today = new Date();
// if (today.isDstObserved()) { 
//   alert ("Daylight saving time!");
// }